<!-- Page Banner -->
<?php if($src = get_post_meta($post->ID,'_page_header_image',true)) {$src = wp_get_attachment_image_src($src,'full'); $header_image = $src[0];} else $header_image = get_bloginfo( 'template_url' ).'/images/about-us-banner.jpg';?>
<section id="page-banner-container" style="background: transparent url(<?php echo $header_image; ?>) no-repeat center center;">
    <article id="page-banner">
        <h1><?php echo str_replace(' ','<br />',$post->post_title);?></h1>
    </article>
</section>


<!-- Content Full -->
<section id="container" class="full">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <?php while(have_posts()): the_post();?>
                <?php the_content();?>
            <?php endwhile;?>
        
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>