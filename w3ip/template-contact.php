<?php
/**
 * Template Name: Contact
 */
?>
<?php get_header(); ?>
<!-- Page Banner -->
<?php if($src = get_post_meta($post->ID,'_page_header_image',true)) {$src = wp_get_attachment_image_src($src,'full'); $header_image = $src[0];} else $header_image = get_bloginfo( 'template_url' ).'/images/contact-banner.jpg';?>
<section id="page-banner-container" style="background: transparent url(<?php echo $header_image; ?>) no-repeat center center;">

    <article id="page-banner">
        <h1>Contact</h1>
    </article>
</section>


<!-- Content Padding Left -->
<section id="container" class="padding-left">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <!-- Column -->
            <div class="column">
                <?php while(have_posts()): the_post();?>
                    <?php the_content();?>
                <?php endwhile;?>
            </div>
            
            <!-- Column Last -->
            <div class="column last">
                
                <!-- Contact Form -->
                <div class="contact-form-container">
                    <h2>Talk <br />to us</h2>
                    <div class="contact-form">
                        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
                    </div>
                </div>
                
            </div>
        
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>
<?php get_footer(); ?>