<?php
function adminInit(){
    //createPostType('test', 'Book', 'Books');
    //createTaxonomy('test-category','Test',"tests",'test');
    
}
add_action('init','adminInit');

add_filter('show_admin_bar', '__return_false');
function pr($var){
	echo "<pre>";print_r($var);echo "</pre>";
}

add_shortcode('left','left_sc');
function left_sc($atts,$content = null){
    return '<div class="column">' . do_shortcode($content) . '</div>';
}

add_shortcode('right','right_sc');
function right_sc($atts,$content = null){
    return '<div class="column last">' . do_shortcode($content) . '</div><div class="clear"><!-- --></div>';
}

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

add_image_size('functions-80-thumbnails',80,80,1); //do not remove this. ever.
add_image_size('news',300,300,1);
add_image_size('news-large',776);

/** Add active classes to custom post types */

function custom_post_type_classes($classes = array(), $menu_item = false){
    global $wp_query;
    if ( $menu_item->object_id == 6 && get_post_type() == 'services' ) { //page id && post type
        $classes[] = 'current-menu-item';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'custom_post_type_classes', 10, 2 );


/** Dropdown Menus */

class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{
    public function start_lvl(&$output, $depth){}
    public function end_lvl(&$output, $depth){}
    public function start_el(&$output, $item, $depth, $args){
      $item->title = str_repeat("&nbsp;", $depth * 4) . $item->title;        
      parent::start_el($output, $item, $depth, $args);        
        $option =  '<option';
        $output = str_replace(array('<li', "><a href=", "</a>"), array($option,' value=','</option>'), $output);
    }
    public function end_el(&$output, $item, $depth){
    }
}

function character_limiter($str, $n = 500, $end_char = '...'){
	if (strlen($str) < $n){
		return $str;
	}
	$str = preg_replace("/\s+/", ' ', preg_replace("/(\r\n|\r|\n)/", " ", $str));
	if (strlen($str) <= $n){
		return $str;
	}
	$out = "";
	foreach (explode(' ', trim($str)) as $val){
		$out .= $val.' ';			
		if (strlen($out) >= $n){
			return trim($out).$end_char;
		}		
	}
}

function get_top_parent_page_id($ID) { 
    $post = get_post($ID);
    $ancestors = $post->ancestors;
    if ($ancestors) { 
        return end($ancestors);
    } else {
        return $post->ID;
    }
}

//*****************************************************/
// Enqueue Scripts
//*****************************************************/
function vrb_load_resources(){
	$template_url = get_template_directory_uri();	
    wp_enqueue_media();
    
    wp_enqueue_script('theme-settings', $template_url.'/include/backend.js', array('jquery'));
    wp_enqueue_style('theme-settings', $template_url.'/include/backend.css');
}
add_action('admin_enqueue_scripts', 'vrb_load_resources');

require ('include/customPostTypes.php');
require ('include/metaBoxClass.php');
require ('include/metaBoxes.php');
require ('include/theme-options.php');
require ('functions-wp.php');

function slick_custom_theme_enqueue_styles() {
    wp_enqueue_style( 'style_slick', get_stylesheet_directory_uri() . '/slick/slick.css' ); 
    wp_enqueue_script( 'slick_js', get_stylesheet_directory_uri().'/slick/slick.js',array());
    wp_enqueue_script( 'custom_js', get_stylesheet_directory_uri().'/slick/custom-slick.js', array());
}
add_action( 'wp_enqueue_scripts', 'slick_custom_theme_enqueue_styles', 11 );


?>