<!-- Page Banner -->
<?php if($src = get_post_meta($post->ID,'_page_header_image',true)) {$src = wp_get_attachment_image_src($src,'full'); $header_image = $src[0];} else $header_image = get_bloginfo( 'template_url' ).'/images/services-banner.jpg';?>
<section id="page-banner-container" style="background: transparent url(<?php echo $header_image; ?>) no-repeat center center;">
    <article id="page-banner">
        <h1><?php echo $post->post_title;?></h1>
    </article>
</section>


<?php get_sidebar('breadcrumbs');?>


<!-- Content Sidebar Left -->
<section id="container" class="has-sidebar-left">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <!-- Sidebar Left -->
            <aside id="sidebar-left">
            
                <!-- Sidebar Nav -->
                <nav id="sidebar-nav">
                    <ul id="sidebar-menu">
                        <?php $items = get_posts( array('post_type' => 'page', 'post_parent' => $post->post_parent, 'numberposts' => -1) ); foreach($items as $item):?>
                        <li <?php if($post->ID == $item->ID):?> class="current-menu-item"<?php endif;?>><a href="<?php echo get_permalink($item->ID);?>"><?php echo $item->post_title;?></a></li>
                        <?php endforeach;?>
                    </ul>
                </nav>
                
            </aside>
            
            <?php while(have_posts()): the_post();?>
                <?php the_content();?>
            <?php endwhile;?>
            
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>