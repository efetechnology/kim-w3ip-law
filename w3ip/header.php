<!DOCTYPE html>

<!--[if IE 7]>

<html class="ie ie7" <?php language_attributes(); ?>>

<![endif]-->

<!--[if IE 8]>

<html class="ie ie8" <?php language_attributes(); ?>>

<![endif]-->

<!--[if !(IE 7) | !(IE 8)  ]><!-->

<html <?php language_attributes(); ?>>

<!--<![endif]-->

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<meta name="apple-mobile-web-app-capable" content="yes" />

<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="shortcut icon" href="https://www.w3iplaw.com.au/wp-content/uploads/2016/06/favicon.ico" />

<link rel="profile" href="https://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascript/jquery-1.11.0.min.js"></script>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascript/site.js"></script>

<!--[if lt IE 9]>

    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascript/html5.js"></script>

    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/javascript/selectivizr-min.js"></script>

<![endif]-->    

<?php wp_head(); ?>



<link href=”https://plus.google.com/110666991331563779076/” rel=”publisher” />



<meta name="google-site-verification" content="z79PoNQpe8gtbEz8H3K95r7WANJCu5yrb__xWsLMjEw" />



<meta name="msvalidate.01" content="11DA927A20710C74F257D003C3574F2B" />



</head>



<body oncontextmenu="return false" oncopy="return false" oncut="return false" <?php body_class(); ?>>

<div id="wrapper">



    <!-- Home Header -->

    <section id="header-container"<?php if(is_front_page()):?> class="home" style="background: transparent url(<?php bloginfo( 'template_url' ); ?>/images/home-banner-1.jpg) no-repeat center center;"<?php endif;?>>

        <header id="header">

            

            <!-- Logo -->

            <a id="logo" href="<?php bloginfo( 'url' ); ?>/"><img alt="Intellectual Property Lawyer Gold Coast" src="<?php bloginfo( 'template_url' ); ?>/images/logo<?php if(is_front_page()):?>-white<?php endif;?>.png" /></a>

            

            <!-- Phone -->

            <div id="phone">

                <p><abbr>Call now</abbr> <strong><span></span><a href="tel:1300776614">1300 77 66 14</a></strong></p>

            </div>

            

            <!-- Menu -->

            <nav id="nav">

                <ul id="menu">

                    <?php wp_nav_menu( array('menu' => 3, 'container' => false, 'items_wrap' => '%3$s') ); ?>

                </ul>

            </nav>

            

            <!-- Mobile Menu Button -->

            <a id="mobile-menu-button" href="#"><span class="lines"><span class="line"><!-- --></span><span class="line"><!-- --></span><span class="line"><!-- --></span></span>Menu</a>

            

            <!-- Mobile Menu -->

            <nav id="mobile-nav">

                <ul id="mobile-menu">

                    <?php wp_nav_menu( array('menu' => 3, 'container' => false, 'items_wrap' => '%3$s') ); ?>

                </ul>

            </nav>

            

            <div class="clear"><!-- --></div>

            <?php if(is_front_page()):?>

            <!-- Header Page Title -->

            <div id="header-page-title">

            

                <!-- Left -->

                <div class="left">

                    <h1><?php echo get_post_meta($post->ID,'_home_heading_1',true);?></h1>

                    <h2><?php echo get_post_meta($post->ID,'_home_heading_2',true);?></h2>

                </div>

                

                <!-- Right -->

                <div class="right">

                    <p><?php echo get_post_meta($post->ID,'_home_tagline',true);?></p>

                </div>

            

                <div class="clear"><!-- --></div>

            </div>

            

            <!-- Home Boxes -->

            <div id="home-boxes">

                <?php for($i=1;$i<=3;$i++):?>

                <!-- Box -->

                <div class="box">

                    <a class="button" href="#"><span class="text"><span class="title"><?php echo get_post_meta($post->ID,'_box_'.$i.'_title',true);?></span><span class="description"><?php echo get_post_meta($post->ID,'_box_'.$i.'_description',true);?></span></span></a>

                    <div class="more">

                        <a href="#" class="close"><!-- --></a>

                        <ul>

                            <?php $menu = wp_get_nav_menu_object(get_post_meta($post->ID,'_box_'.$i.'_menu_left',true));?>

                            <li><?php echo $menu->name;?></li>

                            <?php wp_nav_menu( array('menu' => get_post_meta($post->ID,'_box_'.$i.'_menu_left',true), 'container' => false, 'items_wrap' => '%3$s') ); ?>

                        </ul>

                        <ul>

                            <?php $menu = wp_get_nav_menu_object(get_post_meta($post->ID,'_box_'.$i.'_menu_right',true));?>

                            <li><?php echo $menu->name;?></li>

                            <?php wp_nav_menu( array('menu' => get_post_meta($post->ID,'_box_'.$i.'_menu_right',true), 'container' => false, 'items_wrap' => '%3$s') ); ?>

                        </ul>

                    </div>

                </div>

                <?php endfor;?>

                

                <!-- Slogan -->

                <div id="slogan">

                    <img alt="Internet lawyer Sydney" src="<?php bloginfo('template_url'); ?>/images/slogan.png" />

                </div>

                

                <div class="clear"><!-- --></div>

            </div>

            

            <!-- ScrollDown -->

            <a class="scrollDown" href="#container"><img alt="Intellectual Property Gold Coast" src="<?php bloginfo( 'template_url' ); ?>/images/scrolldown-arrow.png" /></a>

            <?php endif;?>

            <div class="clear"><!-- --></div>

        </header>

    </section>

