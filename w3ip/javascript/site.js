document.onmousedown=disableclick;
function disableclick(event)
{
    if(event.button==2)
{
     return false;    
}
};

$(window).load(function() {
    /***** Transitional Effects *****/
    setTimeout(function(){
        $("body a, body input[type='submit'], button").addClass("loaded");    
    },1000);
    
    $("#phone").css('width',$("#nav").width());
});

$(document).ready(function(){
    /***** Home Boxes *****/
    if($("#home-boxes").length > 0){
        
        $("#home-boxes .button").bind('touchstart',function(e){
            e.preventDefault();
            $this = $(this);
            if($this.hasClass('active')){
                $this.removeClass('active');
                $this.parent().find('.more').slideUp();   
            } else {
                $("#home-boxes .button.active").removeClass('active');
                $("#home-boxes .more").slideUp();
                $this.addClass('active');
                $this.parent().find('.more').slideDown();
            };
        }); 
        
        $("#home-boxes .box").mouseover(function(){
            $this = $(this);
            $("#home-boxes .button.active").stop().removeClass('active');
            $("#home-boxes .more").stop().slideUp();
            $this.find('a').stop().addClass('active');
            $this.find('.more').stop().slideDown();           
        });  
        
        $("#home-boxes .box").mouseleave(function(){
            $this = $(this);
            $this.find('a').stop().removeClass('active');
            $this.find('.more').stop().slideUp();   
        });  
        
        $("#home-boxes .close").click(function(e){
            e.preventDefault();
            $(this).parent().slideUp();
            $(this).parent().parent().find('.button').removeClass('active');
        });  
        
        $("#phone").css('width',$("#nav").width());
        
        if($(window).width() > 1200){
            $("#phone a").click(function(e){
                e.preventDefault()
            });
        };
    };
    
    
    /***** Home Header Container *****/
    if($(".home #header-container").length > 0){
        if($(window).width() > 1200){
            $(".home #header-container").css('min-height',$(window).height()-48);    
        } else {
            $(".home #header-container").css('min-height',$(window).height()-30);            
        };
    };
    
    
    
    /***** Content Padding Top *****/
    if($("#page-title").length > 0){
        $("#container.padding-left #content").css('padding-top',$("#page-title").height());  
    };
    
    
    /***** ScrollDown *****/
    if($(".scrollDown").length > 0){
        $(".scrollDown").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top}, 500);
        });  
    };
    
    
    /***** Mobile Menu *****/
    $("#mobile-menu-button").click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $("#mobile-menu").stop().slideToggle(); 
    });
    
    $("#mobile-menu li").each(function(){
        if($(this).find('ul').length > 0){
            $(this).addClass('dropdown');
        };
    });

    $("#mobile-menu > li.dropdown > a").click(function(e){
        if(!$(this).hasClass('open')){
            e.preventDefault();
            $("#mobile-menu li ul").slideUp();
            $("#mobile-menu li a").removeClass('open')
            $(this).parent().find('ul').slideDown();
            $(this).addClass('open');
        };
    });
});

$(window).bind('load resize',function(){
    /***** Home Header Container *****/
    if($(".home #header-container").length > 0){
        if($(window).width() > 1200){
            $(".home #header-container").css('min-height',$(window).height()-48);    
        } else {
            $(".home #header-container").css('min-height',$(window).height()-30);            
        };
    };
    
    
    /***** Content Padding Top *****/
    if($("#page-title").length > 0){
        $("#container.padding-left #content").css('padding-top',$("#page-title").height());  
    };
    
    
    /***** Services List *****/
    if($("#services").length > 0){
        $('#services .service a').each(function(){
            $this = $(this);
            $this.find('.text').css('padding-top',($this.height()-$this.find('.text').height())/2)
        });
    };
    
    
    /***** Container Has Sidebar Left *****/
    if($("#sidebar-left").length > 0){
        $('#container.has-sidebar-left #content').css('min-height',$("#sidebar-left").outerHeight(true));
    };
});

jQuery.fn.adjustHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).height() > maxHeightFound ) {
            maxHeightFound = $(this).height();
        }
    });
    this.css('min-height',maxHeightFound);
};