<?php get_header(); ?>
<!-- Page Banner -->
<section id="page-banner-container" style="background: transparent url(<?php bloginfo( 'template_url' ); ?>/images/news-banner.jpg) no-repeat center center;">
    <article id="page-banner">
        <h1><?php echo single_cat_title( '', false ) ?></h1>
    </article>
</section>


<?php get_sidebar('breadcrumbs');?>


<!-- Content Sidebar Right -->
<section id="container" class="has-sidebar-right">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <!-- Posts -->
            <div id="posts">
                <?php while(have_posts()): the_post();?>
                <?php if(get_post_meta($post->ID,'_post_event',true) == 'on'):?>
                <div class="post event">
                    <?php if(has_post_thumbnail()):?>
                    <!-- Post Thumb -->
                    <div class="post-thumb">
                        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('news');?></a>
                    </div>
                    <?php endif;?>
                    <span class="post-event-date">EVENT &nbsp;&nbsp;|&nbsp;&nbsp; <strong><?php echo date('jS F, Y',get_post_meta($post->ID,'_post_date',true));?></strong></span>
                    <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                    <?php the_excerpt();?>
                    <a href="<?php the_permalink();?>">Read more...</a>
                    
                </div>
                <?php else:?>
                <!-- Post -->
                <div class="post">
                    
                    <?php if(has_post_thumbnail()):?>
                    <!-- Post Thumb -->
                    <div class="post-thumb">
                        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('news');?></a>
                    </div>
                    <?php endif;?>
                    
                    <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                    <span class="post-date"><?php echo get_the_date('jS F Y', $post->ID);?> - <?php the_author_link(); ?></span>
                    <?php the_excerpt();?>
                    <a href="<?php the_permalink();?>">Read more...</a>
                    
                </div>
                <?php endif;?>
                <?php endwhile;?>
                                
            </div>
            
            <!-- Page Navigation -->
            <div id="page-navigation">
                
                <?php wp_pagenavi();?>
                
            </div>
            
            <?php get_sidebar(); ?>
            
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>

<?php get_footer(); ?>