<?php get_header(); ?>

<!-- Content Full -->
<section id="container" class="full">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <h1>WHOOPS, SORRY</h1>
            <h3>This page was not found.</h3>
        
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>
<?php get_footer(); ?>