<!-- Footer -->
<section id="footer-container">
    <footer id="footer">
        
        <!-- Left Column -->
        <div class="column left">
            <?php echo apply_filters('the_content', get_site_option('footer_content'));?>
        </div>
        
        <!-- Right Column -->
        <div class="column right">
            
            <!-- Phone Numbers -->
            <div class="phone-numbers">
                <p><strong>Sydney:</strong> <a href="tel:<?php echo str_replace(' ','',get_site_option('phone_number_sydney'));?>"><?php echo get_site_option('phone_number_sydney');?></a></p>
                <p><strong>Gold Coast:</strong> <a href="tel:<?php echo str_replace(' ','',get_site_option('phone_number_gold_coast'));?>"><?php echo get_site_option('phone_number_gold_coast');?></a></p>
            </div>
            
            <!-- Social Icons -->
            <div class="social-icons">
                <a href="<?php echo get_site_option('facebook');?>" target="_blank" class="facebook"><!-- --></a>
                <a href="<?php echo get_site_option('twitter');?>" target="_blank" class="twitter"><!-- --></a>
                <a href="<?php echo get_site_option('linkedin');?>" target="_blank" class="linkedin"><!-- --></a>
                <a href="<?php echo get_site_option('googleplus');?>" target="_blank" class="googleplus"><!-- --></a>
                <div class="clear"><!-- --></div>
            </div>
            
            <div class="clear"><!-- --></div>
            
            <!-- Footer Menu -->
            <nav id="footer-nav">
                <ul id="footer-menu">
                    <?php wp_nav_menu( array('menu' => 2, 'container' => false, 'items_wrap' => '%3$s') ); ?>
                </ul>
                <div class="clear"><!-- --></div>
            </nav>
            
            <!-- Credits -->
            <a rel="nofollow" id="credits" href="http://www.divinewrite.com.au/" target="_blank">Divine Write Website</a>
            
            <div class="clear"><!-- --></div>
        </div>
    
        <div class="clear"><!-- --></div>
    </footer>
</section>

</div>    
<?php wp_footer(); ?>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-72308832-1', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->

</body>
</html>