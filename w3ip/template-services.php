<?php
/**
 * Template Name: Services
 */
?>
<?php get_header(); ?>
<!-- Page Banner -->
<?php if($src = get_post_meta($post->ID,'_page_header_image',true)) {$src = wp_get_attachment_image_src($src,'full'); $header_image = $src[0];} else $header_image = get_bloginfo( 'template_url' ).'/images/services-banner.jpg';?>
<section id="page-banner-container" style="background: transparent url(<?php echo $header_image; ?>) no-repeat center center;">
    <article id="page-banner">
        <h1>Services</h1>
    </article>
</section>


<!-- Services -->
<div id="services-container">
    <h1>Choose a service</h1>
    
    <!-- Services List -->
    <div id="services">
        <?php $items = get_posts( array('post_type' => 'page', 'post_parent' => $post->ID, 'numberposts' => -1) ); foreach($items as $item):?>

        <!-- Service -->
        <div class="service">
            <a href="<?php echo get_permalink($item->ID);?>"><span class="image"><img alt="Copyright Sydney" src="<?php bloginfo( 'template_url' ); ?>/images/service-icon.png" /></span><span class="text"><?php echo $item->post_title;?></span></a>
        </div>
        <?php endforeach;?>
        

        <div class="clear"><!-- --></div>    
    </div>
    
</div>
<?php get_footer(); ?>