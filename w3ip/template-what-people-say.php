<?php
/**
 * Template Name: What People Say
 */
?>
<?php get_header(); ?>
<!-- Page Banner -->
<?php if($src = get_post_meta($post->ID,'_page_header_image',true)) {$src = wp_get_attachment_image_src($src,'full'); $header_image = $src[0];} else $header_image = get_bloginfo( 'template_url' ).'/images/what-people-say-banner.jpg';?>
<section id="page-banner-container" style="background: transparent url(<?php echo $header_image; ?>) no-repeat center center;">

    <article id="page-banner">
        <h1> Our <br> Clients </h1>
    </article>
</section>


<!-- Content Padding Left -->
<?php $content_image = get_bloginfo( 'template_url' ).'/images/what-people-say-content-bg.jpg'; ?> 
<section id="container" class="padding-left" style="background: transparent url(<?php echo  $content_image; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
    <article id="content">
        
        <!-- Inner -->
        <div class="inner">
            
            <!-- Column -->
            <div class="column multiple-items">
                <?php // echo strip_tags(apply_filters('the_content',get_post_meta(2,'_home_what_people_say',true)), '<blockquote> <cite>') ;?>


                <?php
                   if( have_rows('client_say') ):
                      while ( have_rows('client_say') ) : the_row();
                      $author = get_sub_field('author');
                      $comment = get_sub_field('comment');
                      $first_comment = '';
                      if(strlen($comment) > 161){
                          $pos=strpos($comment, ' ', 160);
                          $first_comment = substr($comment,0,$pos );
                          $last_comment =  substr($comment,$pos);

//                          $first_comment =  substr($comment,0,160);
//                         $last_comment =  substr($comment,160);
                      }
                ?>

                <div>
                    <div class="content-inside">
                       <div>
                        <?php if($first_comment != ''): ?>
                        <p><span class="teaser"><?php echo $first_comment; ?></span><span class="extend">...</span><span class="complete"><?php echo $last_comment; ?></span></p>

                        <p style="font-weight: bold"><?php echo  $author; ?></p>
                        <p class="read-more"><span>More</span></p>
                      <?php else : ?>
                        <p><span class="teaser"><?php echo $comment; ?> </span></p>
                        <p style="font-weight: bold"><?php echo  $author; ?></p>                       
                      <?php endif; ?>
                        </div>
                    </div>
                </div>



                <?php     endwhile;
                else :
                    // no rows found
                endif;
                ?>

<!--                <div>-->
<!--                    <div class="content-inside">-->
<!--                       <div>-->
<!--                        <p><span class="teaser"> Kim was recommended to us by a barrister in Sydney, and she delivered and then some. We had a really tight </span><span class="extend">...</span> <span class="complete">budget and she was able to work within the budget for us. She was lovely to deal with and got straight to the point - which we appreciated a lot. We would definitely recommend and look forward to using her again</span></p>-->
<!--                        <p style="font-weight: bold">Nikki Douglas</p>-->
<!--                        <p class="read-more"><span>More</span></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--               <div>-->
<!--                   <div class="content-inside">-->
<!--                     <div>-->
<!--                       <p><span class="teaser">Kim and her team are excellent. She has through the years provided me with consistently good trademark advice and </span><span class="extend">...</span><span class="complete">supported my business and its goals</span></p>-->
<!--                       <p style="font-weight: bold">Norah Cullen</p>-->
<!--                        <p class="read-more"><span>More</span></p>-->
<!--                       </div>-->
<!--                       </div>-->
<!--               </div>-->
<!--               <div>-->
<!--                   <div class="content-inside">-->
<!--                        <div>-->
<!--                           <p><span class="teaser">W3IP Law helped me with a cease and desist letter and advised me on a range of different legal matters </span><span class="extend">...</span><span class="complete">relating to intellectual property. They prepared me well and the fees were explained to me with all the options available. I would have no hesitation recommending them</span></p>-->
<!--                           <p style="font-weight: bold">Paul Mitchell</p>-->
<!--                            <p class="read-more"><span>More</span></p>-->
<!--                       </div>-->
<!--                       </div>-->
<!--               </div>-->






                   
            </div>
            
            <!-- Column Last -->
            <div class="column last">
                
                <!-- Contact Form -->
                <div class="contact-form-container">
                    <h2>Talk <br />to us</h2>
                    <div class="contact-form">
                        <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
                    </div>
                </div>
                
            </div>
        
            <div class="clear"><!-- --></div>
        </div>
        
    </article>
</section>
<?php get_footer(); ?>