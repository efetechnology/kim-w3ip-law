<?php get_header(); ?>
    <!-- Content Padding Left -->
    <section id="container" class="padding-left">
        <article id="content">
        
            <!-- Page Title -->
            <div id="page-title">
                <h3 style="color: #0e122e; font-family: GothamBlack; font-size: 75px !important; font-weight: normal; letter-spacing: 0 !important; line-height: 61px !important;
    text-transform: uppercase;">WHAT <br />WE DO</h3>
            </div>
        
            <div class="clear"><!-- --></div>
            
            <!-- Inner -->
            <div class="inner">
                
                <!-- Column -->
                <div class="column">
                    <?php echo apply_filters('the_content',get_post_meta($post->ID,'_home_what_we_do',true)) ;?>
                </div>
                
                <!-- Column Last -->
                <div class="column last">
                    <!-- Contact Form -->
                    <div class="contact-form-container">
                        <h2>Talk <br />to us</h2>
                        <div class="contact-form">
                            <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
                        </div>
                    </div>
                   
                    
                </div>
            
                <div class="clear"><!-- --></div>
            </div>
            
        </article>
    </section>
    
    <!-- Content Padding Left -->
    <section id="container" class="padding-left how-we-do-it" style="background: transparent url(<?php bloginfo('template_url'); ?>/images/home-how-bg-image.jpg) no-repeat left center; background-size:cover;">
        <article id="content">
        
            <!-- Inner -->
            <div class="inner">
                
                <!-- Column Last -->
                <div class="column last">
                    
                    <!-- Page Title -->
                    <div id="page-title">
                        <h3 style="color: #fff; padding-bottom: 36px; line-height: 61px !important; font-size: 75px !important; font-family: GothamBlack;">HOW <br />WE <br />DO IT</h3>
                    </div>
                    
                    <?php echo apply_filters('the_content',get_post_meta($post->ID,'_home_how_we_do_it',true)) ;?>
                                        
                    
                </div>
            
                <div class="clear"><!-- --></div>
            </div>
            
        </article>
    </section>
    
    <!-- Content Padding Left -->
    <section id="container" class="padding-left what-it-costs">
        <article id="content">
        
            <!-- Inner -->
            <div class="inner">
                
                <!-- Column Last -->
                <div class="column last">
                    
                    <!-- Page Title -->
                    <div id="page-title">
                        <h3 style="color: #0e122e; font-family: GothamBlack; font-size: 75px !important; font-weight: normal; letter-spacing: 0 !important; line-height: 61px !important; text-transform: uppercase;">WHAT <br />IT <br />COSTS</h3>
                    </div>
                            
                    <?php echo apply_filters('the_content',get_post_meta($post->ID,'_home_what_it_costs',true)) ;?>
                </div>
            
                <div class="clear"><!-- --></div>
            </div>
            
        </article>
    </section>
    
    <section id="container" class="padding-left what-people-say">
        <article id="content">
        
            <!-- Page Title -->
            <div id="page-title">
                <h3 style="color: #0e122e; font-family: GothamBlack; font-size: 75px !important; font-weight: normal; letter-spacing: 0 !important; line-height: 61px !important; text-transform: uppercase;">WHAT <br />PEOPLE <br />SAY</h3>
            </div>
        
            <!-- Inner -->
            <div class="inner">
                
                
                <!-- Column Last -->
                <div class="column last">
                    <?php echo strip_tags(apply_filters('the_content',get_post_meta($post->ID,'_home_what_people_say',true)), '<blockquote> <cite>') ;?>
                </div>
            
                <div class="clear"><!-- --></div>
            </div>
            
        </article>
    </section>
    
    <section id="container" class="padding-left about-us" style="background: transparent url(<?php bloginfo('template_url'); ?>/images/home-about-bg-image.jpg) no-repeat center center; background-size:cover;">
        <article id="content">
        
            <!-- Page Title -->
            <div id="page-title">
                <h3 style="color: #0e122e; font-family: GothamBlack; font-size: 75px !important; font-weight: normal; letter-spacing: 0 !important; line-height: 61px !important; text-transform: uppercase;">About <br />Us</h3>
            </div>
        
            <div class="clear"><!-- --></div>
            
            <!-- Inner -->
            <div class="inner">
                
                <!-- Column -->
                <div class="column">
                    <?php echo apply_filters('the_content',get_post_meta($post->ID,'_home_about_us',true)) ;?>
                </div>
                
                <!-- Column Last -->
                <div class="column last">
                    <!-- Contact Form -->
                    <div class="contact-form-container">
                        <h2>Talk <br />to us</h2>
                        <div class="contact-form">
                            <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
                        </div>
                    </div>
                   
                    
                </div>
            
                <div class="clear"><!-- --></div>
            </div>
            
        </article>
    </section>
<?php get_footer(); ?>