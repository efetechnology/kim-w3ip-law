<?php get_header(); ?>
<!-- Page Banner -->
<section id="page-banner-container" style="background: transparent url(<?php bloginfo( 'template_url' ); ?>/images/news-banner.jpg) no-repeat center center;">
    <article id="page-banner">
        <h1>News &amp; <br />Events</h1>
    </article>
</section>


<?php get_sidebar('breadcrumbs');?>


<!-- Content Sidebar Right -->
<section id="container" class="has-sidebar-right">
    <article id="content">

        <!-- Inner -->
        <div class="inner">

            <!-- Posts -->
            <div id="posts">

                <!-- Post -->
                <div class="post">

                    <!-- Post Thumb -->
                    <div class="post-thumb">
                        <!-- -->
                    </div>

                    <span class="post-detail-thumb"><?php the_post_thumbnail('news-large');?></span>
                    <h1><?php the_title();?></h1>
                    <?php if(get_post_meta($post->ID,'_post_event',true) == 'on'):?>
                    <span class="post-event-date">EVENT &nbsp;&nbsp;|&nbsp;&nbsp; <strong><?php echo date('jS F, Y',get_post_meta($post->ID,'_post_date',true));?></strong></span>
                    <?php else:?>
                    <span class="post-date"><?php echo get_the_date('jS F Y', $post->ID);?> - <?php the_author_link(); ?></span>
                    <?php endif;?>


                    <?php while(have_posts()): the_post();?>
                        <?php the_content();?>
                    <?php endwhile;?>

                    <div class="share-post">
                        <p>Share this article</p>
                        <a href="mailto:?&body=<?php the_permalink();?>" target="_blank" class="email"><!-- --></a>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>" target="_blank" class="facebook"><!-- --></a>
                        <a href="https://twitter.com/home?status=<?php the_title();?>%20-%20<?php the_permalink();?>" target="_blank" class="twitter"><!-- --></a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>&title=<?php the_title();?>&summary=&source=" target="_blank" class="linkedin"><!-- --></a>
                        <a href="https://plus.google.com/share?url=<?php the_permalink();?>" target="_blank" class="googleplus"><!-- --></a>
                    </div>

                    <?php comments_template();?>

                </div>



            </div>

            <?php get_sidebar(); ?>

            <div class="clear"><!-- --></div>
        </div>

    </article>
</section>


<?php get_footer(); ?>
