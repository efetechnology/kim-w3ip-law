<!-- Sidebar Left -->
<aside id="sidebar-right">

    <!-- Newsletter -->
    <div id="newsletter">
        <h2>Subscribe to our newsletter</h2>
        <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');?>
        <?php /*
        <form>
            <label>Email Address</label>
            <input type="email" />
            <input type="submit" value="Subscribe" />    
        </form>
        */?>
    </div>

    <h2>Categories</h2>

    <!-- Sidebar Nav -->
    <nav id="sidebar-nav">
        <ul id="sidebar-menu">
            <?php wp_list_categories('title_li=');?>
        </ul>
    </nav>
    
</aside>