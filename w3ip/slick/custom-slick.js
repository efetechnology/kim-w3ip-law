
(function($){
    $(document).ready(function(){


        <!-- Start of LiveChat (www.livechatinc.com) code -->

        window.__lc = window.__lc || {};
        window.__lc.license = 8962979;
        (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();

        <!-- End of LiveChat code -->


    	var $slider = $(".multiple-items");		

		$slider.slick({
			  infinite: true,
			  slidesToShow: 2,
			  slidesToScroll: 1,
			  autoplay: true,
              autoplaySpeed: 2000,
			  prevArrow:"<button class='btn-left'><img class='a-left control-c prev slick-prev' src='https://www.w3iplaw.com.au/wp-content/themes/w3ip/images/arrow-left.png'></button>",
     		  nextArrow:"<button class='btn-right'><img class='a-right control-c next slick-next' src='https://www.w3iplaw.com.au/wp-content/themes/w3ip/images/arrow-right.png'></button>",
     		  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false
			      }
			    }

			  ]

			});
			$slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  					var a = $('.btn-left.slick-arrow.loaded');
		        var b = $('.btn-right.slick-arrow.loaded');
		        b.after(a);
			  });



            // setTimeout(function(){
            // 	var a = $('.btn-left.slick-arrow.loaded');
		     //    var b = $('.btn-right.slick-arrow.loaded');
		     //    b.after(a);
            //
		     //     }, 2000);

        $( ".slick-slide" ).height('330px');

			$(".read-more span").on('click',function(){



				if($(this).closest('p').siblings('p').children('.complete').is(":visible")){
				  $(this).text("More").closest('p').siblings('p').children('.complete').hide();
				  $(this).closest('p').siblings('p').children('.extend').show();
                    $( ".slick-slide" ).height( '330px' );
				}else{
					$('.complete:visible').closest('p').siblings('p').children('span').text('More');
                    $('.complete').hide();
					  $(this).text("Less").closest('p').siblings('p').children('.complete').show();
                    $( ".slick-slide" ).height( $(this).closest('.content-inside').height() );

					    $(this).closest('p').siblings('p').children('.extend').hide();





				}
				
			});
			
			
			
            
});
})(jQuery);