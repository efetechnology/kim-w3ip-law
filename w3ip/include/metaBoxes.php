<?php

$menus  = get_terms( 'nav_menu', array( 'hide_empty' => true ) ); foreach($menus as $m){
    $menusDropdown[$m->term_id] = $m->name;
}
$prefix = '_';
global $meta_boxes;
$meta_boxes = array(
    array(
	    'id'		=> 'home-meta-box',
	    'title' 	=> 'Options',
	    'pages'     => array('page'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Heading 1',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_heading_1',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Heading 2',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_heading_2',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Tagline',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_tagline',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'What We Do',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_what_we_do',
	        	'type' 	=> 'editor'
	        ),
            array(
	        	'name' 	=> 'How We Do It',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_how_we_do_it',
	        	'type' 	=> 'editor'
	        ),
            array(
	        	'name' 	=> 'What It Costs',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_what_it_costs',
	        	'type' 	=> 'editor'
	        ),
            array(
	        	'name' 	=> 'What People Say',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_what_people_say',
	        	'type' 	=> 'editor'
	        ),
            array(
	        	'name' 	=> 'About Us',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'home_about_us',
	        	'type' 	=> 'editor'
	        )
        )
    ),
    array(
	    'id'		=> 'page-meta-box',
	    'title' 	=> 'Options',
	    'pages'     => array('page'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Header Image',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'page_header_image',
	        	'type' 	=> 'image'
	        )
        )
    ),
    array(
	    'id'		=> 'post-meta-box',
	    'title' 	=> 'Options',
	    'pages'     => array('post'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Make this an event',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'post_event',
	        	'type' 	=> 'checkbox'
	        ),
            array(
	        	'name' 	=> 'Event Date',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'post_date',
	        	'type' 	=> 'datepicker'
	        )
        )
    ),
    array(
	    'id'		=> 'home-meta-box-1',
	    'title' 	=> 'Box 1',
	    'pages'     => array('page'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Title',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_1_title',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Description',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_1_description',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Left Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_1_menu_left',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        ),
            array(
	        	'name' 	=> 'Right Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_1_menu_right',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        )
        )
    ),
    array(
	    'id'		=> 'home-meta-box-2',
	    'title' 	=> 'Box 2',
	    'pages'     => array('page'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Title',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_2_title',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Description',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_2_description',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Left Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_2_menu_left',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        ),
            array(
	        	'name' 	=> 'Right Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_2_menu_right',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        )
        )
    ),
    array(
	    'id'		=> 'home-meta-box-3',
	    'title' 	=> 'Box 3',
	    'pages'     => array('page'),
	    'context' 	=> 'normal',
	    'priority' 	=> 'low',
	    'fields' 	=> array(
            array(
	        	'name' 	=> 'Title',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_3_title',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Description',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_3_description',
	        	'type' 	=> 'text'
	        ),
            array(
	        	'name' 	=> 'Left Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_3_menu_left',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        ),
            array(
	        	'name' 	=> 'Right Menu',
	        	'desc' 	=> '',
	        	'id' 	=> $prefix.'box_3_menu_right',
	        	'type' 	=> 'select',
                'options' => $menusDropdown
	        )
        )
    )
);

foreach ($meta_boxes as $meta_box) {
    $my_box = new My_meta_box($meta_box);
}
?>
