<?php
function createPostType($postTypeName, $singleLabel, $pluralLabel, $supports = array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )) {
    register_post_type( $postTypeName,  
        array(
            'labels' => array(
                'name' => $pluralLabel,
                'singular_name' => $singleLabel,
                'add_new' => 'Add New',
                'add_new_item' => 'Add New ' . $singleLabel ,
                'edit_item' => 'Edit ' . $singleLabel ,
                'new_item' => 'New ' . $singleLabel ,
                'all_items' => 'All ' . $pluralLabel,
                'view_item' => 'View ' . $singleLabel ,
                'search_items' => 'Search ' . $pluralLabel,
                'not_found' =>  'No ' . $pluralLabel . ' found',
                'not_found_in_trash' => 'No ' . $pluralLabel . ' found in Trash', 
                'parent_item_colon' => '',
                'menu_name' => $pluralLabel
            ),
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true, 
            'show_in_menu' => true, 
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'has_archive' => true, 
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => $supports
        )
    ); 
}


function createTaxonomy($taxonomyName, $singleLabel, $pluralLabel, $postTypes = array()){
    register_taxonomy(
        $taxonomyName,
        $postTypes, 
        array(
            'hierarchical' => true,
            'labels' => 
                array(
                    'name' => $pluralLabel,
                    'singular_name' => $singleLabel,
                    'search_items' =>  'Search ' . $pluralLabel,
                    'all_items' => 'All ' . $pluralLabel,
                    'parent_item' => 'Parent ' . $singleLabel,
                    'parent_item_colon' => 'Parent ' . $singleLabel . ':',
                    'edit_item' => 'Edit ' . $singleLabel, 
                    'update_item' => 'Update ' . $singleLabel,
                    'add_new_item' => 'Add New ' . $singleLabel,
                    'new_item_name' => 'New ' . $singleLabel . ' Name',
                    'menu_name' =>  $singleLabel,
                ),
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true
        )
    );   
}
?>