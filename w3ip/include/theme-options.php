<?php

// Add "vrb Options" link to the "Appearance" menu
function vrb_menu_options() {
	//add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function);
     add_theme_page('Theme Options', 'Theme Options', 'edit_theme_options', 'vrb-settings', 'vrb_admin_options_page');
}
// Load the Admin Options page
add_action('admin_menu', 'vrb_menu_options');

function vrb_admin_options_page() {

		if($_GET['do'] =='save'):
           
            update_site_option('phone_number_sydney', $_POST["phone_number_sydney"]);
            update_site_option('phone_number_gold_coast', $_POST["phone_number_gold_coast"]);
            update_site_option('facebook', $_POST["facebook"]);
            update_site_option('twitter', $_POST["twitter"]);
            update_site_option('linkedin', $_POST["linkedin"]);
            update_site_option('googleplus', $_POST["googleplus"]);

            update_site_option('footer_content', stripslashes($_POST["footer_content"]));

			wp_redirect(admin_url('themes.php?page=vrb-settings'));
		else:
	?>
	
		<div class="wrap">
			
			<div id="icon-themes" class="icon32"><br /></div>
		
			<h2><?php _e( 'Theme Options', 'vrb' ); ?></h2>
		
			<form id="form-vrb-options" action="<?php echo admin_url('themes.php?page=vrb-settings&do=save&noheader=true');?>" method="post" enctype="multipart/form-data">
			
            <div id="poststuff">
                <div class="postbox-container metabox-holder">
                    <div class="meta-box-sortables">
            			<div id="favicon-box" class="postbox">                           
                            <h3 class="hndle"><span>Site Options</span></h3>
                            <div class="inside">
                                <table class="form-table">
                                <tbody>
                                    <tr>
                                        <th scope="row"><label for="phone_number_sydney">Sydney - Phone number</label></th>
                                        <td><input name="phone_number_sydney" type="text" id="phone_number_sydney" value="<?php echo get_site_option('phone_number_sydney');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="phone_number_gold_coast">Gold Coast - Phone number</label></th>
                                        <td><input name="phone_number_gold_coast" type="text" id="phone_number_gold_coast" value="<?php echo get_site_option('phone_number_gold_coast');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="facebook">Facebook</label></th>
                                        <td><input name="facebook" type="text" id="facebook" value="<?php echo get_site_option('facebook');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="twitter">Twitter</label></th>
                                        <td><input name="twitter" type="text" id="twitter" value="<?php echo get_site_option('twitter');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="linkedin">Linkedin</label></th>
                                        <td><input name="linkedin" type="text" id="linkedin" value="<?php echo get_site_option('linkedin');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="googleplus">Google+</label></th>
                                        <td><input name="googleplus" type="text" id="googleplus" value="<?php echo get_site_option('googleplus');?>" class="regular-text"></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row"><label for="footer_content">Footer content</label></th>
                                        <td>
                                            <?php wp_editor( get_site_option('footer_content'), 'footer_content', $settings = array(
                                                'textarea_name' => 'footer_content', 
                                                'media_buttons' => false
                                            ) ); ?> 
                                        </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clear"><!-- --></div>


			<p class="submit">
				<input name="theme_vrb_options[submit]" id="submit_options_form" type="submit" class="button-primary" value="<?php esc_attr_e('Save Settings', 'vrb'); ?>" />
			</p>
			
			</form>
			
		</div>
	<?php
	endif;
}