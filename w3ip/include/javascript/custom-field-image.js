/* Image */
jQuery(document).ready(function($){
    var tgm_media_frame;
    jQuery(document.body).on('click.tgmOpenMediaManager', '.tgm-open-media', function(e){
        e.preventDefault();
        $thisButton = jQuery(this);
    
        if ( tgm_media_frame ) {
            tgm_media_frame.open();
            return;
        }

        tgm_media_frame = wp.media.frames.tgm_media_frame = wp.media({
            className: 'media-frame tgm-media-frame',
            frame: 'select',
            multiple: false,
            title: "Select Image",
            library: {
                type: 'image'
            },
            button: {
                text:  "Insert Image"
            }
        });

        tgm_media_frame.on('select', function(){
            var media_attachment = tgm_media_frame.state().get('selection').first().toJSON();
            $thisButton.parent().find('.tgm-new-media-image').val(media_attachment.id);
            $thisButton.parent().find('.tgm-preview-image').attr("src", media_attachment.url).css("max-width",'200px');
        });

        tgm_media_frame.open();
    });
    
    jQuery('.tgm-clear-image').click(function() {  
        var defaultImage = jQuery(this).parent().parent().find('.custom_default_image').html();  
        jQuery(this).parent().parent().find('.tgm-new-media-image').val('');  
        jQuery(this).parent().parent().find('.tgm-preview-image').attr('src', defaultImage).css('max-width', '200px');  
        jQuery(this).css('display', 'none') 
        return false;  
    }); 
    
});
/* File */
jQuery(document).ready(function($){
    var tgm_media_frame_file;
    jQuery(document.body).on('click.tgmOpenMediaManager', '.tgm-open-file', function(e){
        e.preventDefault();
        $thisButton = jQuery(this);
    
        if ( tgm_media_frame_file ) {
            tgm_media_frame_file.open();
            return;
        }

        tgm_media_frame_file = wp.media.frames.tgm_media_frame_file = wp.media({
            className: 'media-frame tgm-media-frame',
            frame: 'select',
            multiple: false,
            title: "Select File",
            button: {
                text:  "Insert File"
            }
        });

        tgm_media_frame_file.on('select', function(){
            var media_attachment = tgm_media_frame_file.state().get('selection').first().toJSON();
            $thisButton.parent().find('.tgm-new-media-file').val(media_attachment.url);
            $thisButton.parent().find('.tgm-preview-file').text(media_attachment.url)
        });

        tgm_media_frame_file.open();
    });
    
    jQuery('.tgm-clear-file').click(function() {  
        var defaultImage = jQuery(this).parent().parent().find('.custom_default_file').html();  
        jQuery(this).parent().parent().find('.tgm-new-media-file').val('');  
        jQuery(this).parent().parent().find('.tgm-preview-file').text('No File');  
        jQuery(this).css('display', 'none') 
        return false;  
    }); 
    
});

function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status!=404;
}


    $(function() {
        $('.tgm-open-gallery').each(function() {
            $(this).click(function(){
                $parent = $(this).parent();
                var gallerysc = '[gallery ids="' + $parent.find('.tgm-new-gallery').val() + '"]';
                wp.media.gallery.edit(gallerysc).on('update', function(g) {
                    var id_array = [], url_array = [];
                    $.each(g.models, function(id, img) {id_array.push(img.id); url_array.push(img.attributes.url) });
                    $parent.find('.tgm-new-gallery').val(id_array.join(","));
                    $parent.find(".tgm-new-gallery-preview").html('');
                    for(var i=0; i < url_array.length; i++){
    
                        img_src = url_array[i];
                        var extension = img_src.substr( (img_src.lastIndexOf('.')) );
                        img_src_80 = img_src.replace(extension,'-83x83' + extension);
                        if(!UrlExists(img_src_80)) img_src_80 = img_src;
                        $parent.find(".tgm-new-gallery-preview").append("<img src='"+img_src_80+"' width='80' height='80' style='margin-right:10px;' />");
                    }
                });
            })
        });
    });
