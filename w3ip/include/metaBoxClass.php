<?php
if(is_admin()) add_thickbox();

function loadResources(){
	$template_url = get_template_directory_uri();	
    wp_enqueue_media();
	wp_enqueue_script('tgm-nmp-media', 	$template_url.'/include/javascript/custom-field-image.js', array( 'jquery' ));
    wp_enqueue_script('chosen', 	$template_url.'/include/chosen/chosen.jquery.min.js');
    wp_enqueue_style('chosen', 	$template_url.'/include/chosen/chosen.css');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    
        
}
add_action('admin_print_scripts', 'loadResources');

class My_meta_box {
 
    protected $_meta_box;
 
    // create meta box based on given data
    function __construct($meta_box) {
        $this->_meta_box = $meta_box;
        add_action('admin_menu', array(&$this, 'add'));
 
        add_action('save_post', array(&$this, 'save'));
    }
 
    /// Add meta box for multiple post types
    function add() {
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
        foreach ($this->_meta_box['pages'] as $page) {
            if($post_id != 2 && ($this->_meta_box['id'] == 'home-meta-box' || $this->_meta_box['id'] == 'home-meta-box-1' || $this->_meta_box['id'] == 'home-meta-box-2' || $this->_meta_box['id'] == 'home-meta-box-3')) break;
            add_meta_box($this->_meta_box['id'], $this->_meta_box['title'], array(&$this, 'show'), $page, $this->_meta_box['context'], $this->_meta_box['priority']);
        }
    }
 
    // Callback function to show fields in meta box
   function show() {
        global $post;
 
        // Use nonce for verification
        echo '<script>jQuery(document).ready(function(){jQuery(".chosen-select").chosen();})</script>';
        echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
        echo '<style>table.form-table tr:hover {background-color:#eeeeee;} .mceIframeContainer {background-color:#fff;}</style>';
        echo '<table class="form-table">';
 
        foreach ($this->_meta_box['fields'] as $field) {
            // get current post meta data
            $meta = get_post_meta($post->ID, $field['id'], true);
 
            echo '<tr>';
            if(empty($this->_meta_box['hide_labels'])){
                echo $field['type'] != 'title' ? '<th style="width:12%"><label for="'. $field['id']. '">'. $field['name']. '</label></th>' : '<th style="width:12%"><strong><label for="'. $field['id']. '">'. $field['name']. '</label></strong></th>';
            }
            echo '<td>';
            switch ($field['type']) {
                case 'text':
                    echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : '', '" size="30" style="width:97%" />',
                        '<br />', $field['desc'];
                    break;
                case 'datepicker':
                    echo '<input class="datepicker" type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? date('j F Y', $meta) : '', '" size="30" style="width:97%" />',
                        '<br />', $field['desc'];
                    break;
                case 'textarea':
                    echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>',
                        '<br />', $field['desc'];
                    break;
                case 'select':
                    echo '<select style="width:45%;" data-placeholder="Please Select" name="', $field['id'], '" id="', $field['id'], '" class="chosen-select"><option value=""/>';
                    foreach ($field['options'] as $key => $option) {
                        echo '<option', $meta == $key ? ' selected="selected"' : '', ' value="'.$key.'">', $option, '</option>';
                    }
                    echo '</select>';
                    break;
                case 'multiple':
                    echo '<select style="width:53%;" data-placeholder="Please Select" name="', $field['id'], '[]" id="', $field['id'], '" class="chosen-select" multiple="multiple">';
                    foreach ($field['options'] as $key => $option) {
                        str_replace("," . $key . ",", "", $meta, $count);
                        echo '<option', $count != 0 ? ' selected="selected"' : '', ' value="'.$key.'">', $option, '</option>';
                    }
                    echo '</select>';
                    break;
                case 'radio':
                    foreach ($field['options'] as $option) {
                        echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                    }
                    break;
                case 'editor':
	            	wp_editor( $meta ? $meta : $field['std'], $field['id'], $settings = array() );                     
	                break;	
                case 'checkbox':
                    echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
                    break;
                //image upload
                case 'image':
                    echo '<div class="tgm-new-media-settings">';
    				    if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; }
                        else $image =  get_bloginfo('template_url') . '/include/javascript/no-custom-field-image.png';
                        echo '<img src="'.$image.'" style="max-width: 200px" class="tgm-preview-image" alt="" /><br /> ';
                        echo '<input type="hidden" class="tgm-new-media-image" size="70" name="'.$field['id'].'" value="'.$meta.'" />';
                        echo '<span class="custom_default_image" style="display:none">'.get_bloginfo('template_url') . '/include/javascript/no-custom-field-image.png</span>'; 
                        echo '<a href="#" class="tgm-open-media button button-secondary">Choose Image</a>';
                        echo '<small> <a href="#" class="tgm-clear-image">Remove Image</a></small>';  
                        
                    echo '</div>';
                break;
                
                case 'gallery':
                    echo '<input type="hidden" class="tgm-new-gallery" size="70" name="'.$field['id'].'" value="'.$meta.'" />';
                    echo '<div class="tgm-new-gallery-preview">';
                        if ($meta) foreach(explode(",",$meta) as $imageID){
                            $image = wp_get_attachment_image_src($imageID, 'functions-80-thumbnails'); $image = $image[0];
                            echo "<img src='".$image."' width='80' height='80' style='margin-right:10px;' />";
                        }
                        
                    echo '</div>';
                    echo '<a href="#" class="tgm-open-gallery button button-secondary">Edit Gallery</a>';
                    
                    break;
                //file upload
				case 'file':
				    $preview = 'No File';
				    $url = '';  
				    echo '<span class="custom_default_file" style="display:none">'.$default.'</span>';  
				    if ($meta) { $url = $meta; $preview = $meta;  }  
				   
				    echo    '<input name="'.$field['id'].'" type="hidden" class="tgm-new-media-file" value="'.$url.'" /> 
		                	<div class="tgm-preview-file">'.$preview.'</div><br /> 
		                    <input class="tgm-open-file button button" type="button" value="Choose File" /> ';
		            
		            if (!$meta) { $hide_style = "display:none";  }     
		            echo'<br /><small><a href="#" style="'.$hide_style.'" class="tgm-clear-file">Remove File</a></small>';   
                   
            }
            echo     '<td>',
                '</tr>';
        }
 
        echo '</table>';
    }
 
    // Save data from meta box
    function save($post_id) {
        if ( defined('DOING_AJAX') )
			return;
        
        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }
        
        //check revision
        if ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] ){
            return $post_id;
        }
        
        if (wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce'))
            return $post_id;
 
        // check permissions
        if ('page' == @ $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return $post_id;
            }
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }
 
        foreach ($this->_meta_box['fields'] as $field) {
            $old = get_post_meta($post_id, $field['id'], true);
            if($field['type'] == 'multiple'){
                $new = ',';
                if(!empty($_POST[$field['id']]))
                    foreach($_POST[$field['id']] as $singleId)
                        $new = $new . $singleId . ",";
            } elseif($field['type'] == 'datepicker'){
                @$new = strtotime($_POST[$field['id']]);  
            } else {
                @$new = $_POST[$field['id']];    
            }
                
            
            
                
             
            
                if ($new && $new != $old) {
                    update_post_meta($post_id, $field['id'], $new);
                } elseif ('' == $new && $old) {
                    delete_post_meta($post_id, $field['id'], $old);
                }
            
        }
    }
}




?>
